package tests;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import modelo.Celda;
import modelo.LineaDeTres;

public class LineaDeTresTest extends TestCase {
	private Celda cX;
	private Celda cO;
	private Celda c_;
	@Before
	public void setUp() throws Exception {
		cX = new Celda().ponerX();
		cO = new Celda().ponerO();
		c_ = new Celda();
	}
	
	
	@Test
	public void testTieneTodasX(){
		//Arrange
		Celda[] arrC = {cX,cX,cX};
		LineaDeTres l = new LineaDeTres(arrC);
		//Act
		Boolean res = l.tieneTodasX();
		//Assert
		assertTrue(res);
	}
	
	@Test
	public void testTieneTodasO(){
		//Arrange
		Celda[] arrC = {cO,cO,cO};
		LineaDeTres l = new LineaDeTres(arrC);
		//Act
		Boolean res = l.tieneTodasO();
		//Assert
		assertTrue(res);
	}
	
	@Test
	public void testRegistrarSiAunPuedeGanarConXX_(){
		Celda[] arrC = {cX,cX,c_};
		LineaDeTres l = new LineaDeTres(arrC);
		l.registrarSiAunPuedenGanar();
		assertTrue(l.puedeGanarX());
		assertFalse(l.puedeGanarO());
	}
	
	@Test
	public void testRegistrarSiAunPuedeGanarConXXO(){
		Celda[] arrC = {cX,cX,cO};
		LineaDeTres l = new LineaDeTres(arrC);
		l.registrarSiAunPuedenGanar();
		assertFalse(l.puedeGanarX());
		assertFalse(l.puedeGanarO());
	}
	
	@Test
	public void testRegistrarSiAunPuedeGanarConO__(){
		Celda[] arrC = {cO,c_,c_};
		LineaDeTres l = new LineaDeTres(arrC);
		l.registrarSiAunPuedenGanar();
		assertFalse(l.puedeGanarX());
		assertTrue(l.puedeGanarO());
	}
	@Test
	public void testRegistrarSiAunPuedeGanarConX__(){
		Celda[] arrC = {cX,c_,c_};
		LineaDeTres l = new LineaDeTres(arrC);
		l.registrarSiAunPuedenGanar();
		assertTrue(l.puedeGanarX());
		assertFalse(l.puedeGanarO());
	}
	@Test
	public void testRegistrarSiAunPuedeGanarCon___(){
		Celda[] arrC = {c_,c_,c_};
		LineaDeTres l = new LineaDeTres(arrC);
		l.registrarSiAunPuedenGanar();
		assertTrue(l.puedeGanarX());
		assertTrue(l.puedeGanarO());
	}

}
