package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Posicion;

public class PosicionTest {
	private Posicion p1;
	private Posicion p2;
	private Posicion p3;
	private Posicion p4;
	
	@Before
	public void setUp() throws Exception {
		p1 = new Posicion(1,0);
		p2 = new Posicion(1,0);
		p3 = new Posicion(1,1);
		p4=null;
	}

	@Test
	public void testEqualsObjectCasoPositivo() {
		boolean P1IgualQueP1 = p1.equals(p1);
		boolean P1IgualQueP2 = p1.equals(p2);
		assertTrue(P1IgualQueP2);
		assertTrue(P1IgualQueP1);
	}
	
	@Test
	public void testEqualsObjectCasoNegativo() {
		boolean P1IgualQueP3 = p1.equals(p3);
		boolean P1IgualQueP4 = p1.equals(p4);
		assertFalse(P1IgualQueP3);
		assertFalse(P1IgualQueP4);
	}

}
