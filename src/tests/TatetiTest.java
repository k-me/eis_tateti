package tests;

import org.junit.Before;
import org.junit.Test;
import excepciones.*;
import junit.framework.TestCase;
import modelo.Posicion;
import modelo.Tateti;

public class TatetiTest extends TestCase {
	private Tateti t;

	@Before
	public void setUp() {
		t = new Tateti();
	}

	@Test
	public void testPonerX() {
		Posicion p1 = new Posicion(1, 1);
		t.ponerX(p1);
		assertTrue(t.hayXEn(p1));
	}

	@Test
	public void testPoneDosVecesEnUnaCeldaYFalla() {
		Posicion p1 = new Posicion(1, 1);

		t.ponerX(p1);
		try {
			t.ponerO(p1);
		} catch (CeldaRepetidaException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testPoneDosVecesMismaFichaYFalla() {
		Posicion p1 = new Posicion(1, 1);
		Posicion p2 = new Posicion(1, 2);
		t.ponerX(p1);
		try {
			t.ponerX(p2);
		} catch (FichaRepetidaException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testSiUnJugadorTieneLaDiagonal_11_33_gana() {
		t.ponerX(new Posicion(0, 0));
		t.ponerO(new Posicion(0, 1));
		t.ponerX(new Posicion(2, 2));
		t.ponerO(new Posicion(0, 2));
		t.ponerX(new Posicion(1, 1));
		boolean ganoX = t.hayGanador() && t.ganoX();
		assertTrue(ganoX);
	}
	
	@Test
	public void testHayEmpateAntesDeLlenarElTablero(){
		t.ponerX(new Posicion(0,2)); // O|O|X
		t.ponerO(new Posicion(0,1)); // X|X|O
		t.ponerX(new Posicion(1,0)); // O| |X		
		t.ponerO(new Posicion(0,0));
		t.ponerX(new Posicion(1,1));
		t.ponerO(new Posicion(1,2));
		t.ponerX(new Posicion(2,2));
		t.ponerO(new Posicion(2,0));
//		t.ponerX(new Posicion(2,1));
		boolean empate = t.hayEmpate();
		assertTrue(empate);
	}
}
