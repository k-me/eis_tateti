package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Ficha;
import modelo.FichaO;
import modelo.FichaX;

public class FichaTest {
	private Ficha fX1;
	private Ficha fX2;
	private Ficha fO;
	private Ficha fNull;
	
	@Before
	public void setUp() throws Exception {
		fX1 = new FichaX();
		fX2 = new FichaX();
		fO = new FichaO();
		fNull = null;
	}

	@Test
	public void testEqualsObjectCasoPositivo() {
		boolean mismoObjO = fO.equals(fO);
		boolean mismoObjX = fX1.equals(fX1);
		boolean objsEquivalentes = fX1.equals(fX2);
		assertTrue(mismoObjX);
		assertTrue(mismoObjO);
		assertTrue(objsEquivalentes);
	}
	
	@Test
	public void testEqualsObjectCasoNegativo() {
		boolean fXIgualQueFO = fX1.equals(fO);
		boolean fXIgualQueNull = fX1.equals(fNull);
		assertFalse(fXIgualQueFO);
		assertFalse(fXIgualQueNull);
	}

}
