package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.Celda;

public class CeldaTest {
	Celda c;
	@Before
	public void setUp() throws Exception {
		c = new Celda();
	}

	@Test
	public void testPonerX() {
		c.ponerX();
		assertTrue(c.tieneX());
	}

	@Test
	public void testPonerO() {
		c.ponerO();
		assertTrue(c.tieneO());
	}

	@Test
	public void testTieneXCasoNegativo() {
		boolean conCeldaVacia = c.tieneX();
		boolean conCeldaLlena = c.ponerO().tieneX();
		assertFalse(conCeldaVacia && conCeldaLlena);
	}

	@Test
	public void testTieneXCasoPositivo() {
		boolean tieneX = c.ponerX().tieneX();
		assertTrue(tieneX);
	}

	@Test
	public void testTieneOCasoNegativo() {
		boolean conCeldaVacia = c.tieneO();
		boolean conCeldaLlena = c.ponerX().tieneO();
		assertFalse(conCeldaVacia && conCeldaLlena);
	}

	@Test
	public void testTieneOCasoPositivo() {
		boolean tieneO = c.ponerO().tieneO();
		assertTrue(tieneO);
	}
}
