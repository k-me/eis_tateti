package modelo;

public abstract class Ficha {
	
	public abstract Boolean esX();
	public abstract Boolean esO();
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
}
