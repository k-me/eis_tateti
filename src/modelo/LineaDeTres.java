package modelo;

public class LineaDeTres {
	Celda[] celdas = new Celda[3];
	Boolean puedeGanarX = true;
	Boolean puedeGanarO = true;
	
	public LineaDeTres(Celda[] arrCeldas){
		celdas = arrCeldas;
	}
	
	public Boolean tieneTodasX(){
		for(Celda c : celdas){
			if(!c.tieneX()) return false;
		}
		return true;
	}
	
	public Boolean tieneTodasO(){
		for(Celda c : celdas){
			if(!c.tieneO()) return false;
		}
		return true;
	}
	
	public void registrarSiAunPuedenGanar(){
		int cantX=0;
		int cantO=0;
		for(Celda c: celdas){
			if(c.tieneO()) {cantO++;}
			else{cantX=c.tieneX()?cantX+1:cantX;}
		}
		puedeGanarX = cantO==0;
		puedeGanarO = cantX==0;		
	}
	
	public Boolean puedeGanarX(){return puedeGanarX;}
	public Boolean puedeGanarO(){return puedeGanarO;}
	
}
