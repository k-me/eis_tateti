package modelo;

public class Posicion {
	
	private int fila;
	private int columna;
	
	public Posicion(int f , int c){
		this.fila = f;
		this.columna = c;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fila;
		result = prime * result + columna;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Posicion other = (Posicion) obj;
		if (fila != other.getFila())
			return false;
		if (columna != other.getColumna())
			return false;
		return true;
	}
	
	
	
	public int getFila(){
		return fila;
	}
	public int getColumna(){
		return columna;
	}
}
