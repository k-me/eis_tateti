package modelo;

public class Celda {
	private Ficha ficha;
	public Celda ponerX() {
		ficha = new FichaX();
		return this;
	}
	public Celda ponerO() {
		ficha = new FichaO();
		return this;
	}
	public boolean tieneX() {
		return !estaVacia() && ficha.esX();
	}
	public boolean tieneO() {
		return !estaVacia() && ficha.esO();
	}
	public boolean estaVacia() {
		return ficha==null;
	}


}
