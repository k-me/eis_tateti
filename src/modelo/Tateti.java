package modelo;

import java.util.ArrayList;
import java.util.List;
import excepciones.*;

public class Tateti {
	private Celda[][] celdas = new Celda[3][3];
	private List<LineaDeTres> lineasDeTres = new ArrayList<LineaDeTres>();
	private Ficha ultimaFicha = null;
	private Boolean hayGanador = false;
	private Boolean hayEmpate = false;
	private int cantJugadas = 0;

	public Tateti() {
		inicializar();
	}

	private Celda obtenerCelda(Posicion p) {
		return celdas[p.getFila()][p.getColumna()];
	}

	private void inicializar() {
		crearCeldas();
		crearLineasDeTres();
	}

	private void crearCeldas() {
		for (int f = 0; f < 3; f++) {
			for (int c = 0; c < 3; c++) {
				celdas[f][c] = new Celda();
			}
		}
	}

	private void crearLineasDeTres() {
		crearLineasHorizontales();
		crearLineasVerticales();
		crearLineasDiagonales();
	}

	private void crearLineasDiagonales() {
		Celda[] d1 = { celdas[0][0], celdas[1][1], celdas[2][2] };
		Celda[] d2 = { celdas[0][2], celdas[1][1], celdas[2][0] };
		lineasDeTres.add(new LineaDeTres(d1));
		lineasDeTres.add(new LineaDeTres(d2));
	}

	private void crearLineasVerticales() {
		for (int i = 0; i < 3; i++) {
			Celda[] linea = new Celda[3];
			for (int j = 0; j < 3; j++) {
				linea[j] = celdas[j][i];
			}
			lineasDeTres.add(new LineaDeTres(linea));
		}
	}

	private void crearLineasHorizontales() {
		for (int i = 0; i < 3; i++) {
			lineasDeTres.add(new LineaDeTres(celdas[i]));
		}
	}

	private void verificarJugada(Celda c, Ficha f) {
		if (!c.estaVacia())
			throw new CeldaRepetidaException();
		if (ultimaFicha != null && ultimaFicha == f)
			throw new FichaRepetidaException();
		if (hayEmpate())
			throw new JuegoEmpatadoException();
		if (hayGanador())
			throw new JuegoFinalizadoException();
	}

	public void ponerX(Posicion p) {
		Celda c = obtenerCelda(p);
		Ficha f = new FichaX();
		verificarJugada(c, f);
		c.ponerX();
		cantJugadas++;
		this.ultimaFicha = f;
		registrarVictoriaOEmpate();
	}

	public void ponerO(Posicion p) {
		Celda c = obtenerCelda(p);
		Ficha f = new FichaO();
		verificarJugada(c, f);
		c.ponerO();
		cantJugadas++;
		this.ultimaFicha = f;
		registrarVictoriaOEmpate();
	}

	public boolean hayXEn(Posicion p) {
		return obtenerCelda(p).tieneX();
	}

	public boolean hayOEn(Posicion p) {
		return obtenerCelda(p).tieneO();
	}

	public void registrarVictoriaOEmpate() {
		boolean esEmpate = true;
		for (LineaDeTres l : lineasDeTres) {
			l.registrarSiAunPuedenGanar();
			if (esEmpate) {
				boolean podriaGanarX = puedeJugarX() && l.puedeGanarX();
				boolean podriaGanarO = puedeJugarO() && l.puedeGanarO();
				esEmpate = !( podriaGanarO || podriaGanarX );
			}
		if (l.tieneTodasO() || l.tieneTodasX()) {
			hayGanador = true;
		}
		hayEmpate = esEmpate;
	}

	}
	
	public boolean puedeJugarX(){
		return cantJugadas<8 || (cantJugadas==8 && ultimaFicha.esO());
	}
	
	public boolean puedeJugarO(){
		return cantJugadas<8 || (cantJugadas==8 && ultimaFicha.esX());
	}

	public boolean ganoX() {
		return hayGanador && ultimaFicha != null && ultimaFicha.esX();
	}

	public boolean ganoO() {
		return hayGanador && ultimaFicha != null && ultimaFicha.esO();
	}

	public boolean hayGanador() {
		return hayGanador;
	}

	public boolean hayEmpate() {
		return hayEmpate;
	}

}
